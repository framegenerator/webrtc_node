    //http://localhost:3000/?userName=euge&roomName=LOT%20Fans
	var name = getQueryVariable('userName') || 'Anonymous';
	var room = getQueryVariable('roomName') || 'Empty Chatroom';

	//SOCKET
	var socket = io();//io() - function defined in socket.io-x.x.x.js file

	//chatroom header
	jQuery('.room-title').text(room);


 	//listen to the 'connect' event. when we connected to server
 	socket.on('connect', function () {
	 	console.log('Client Connected with socket');

	 	//generating Room wich user picked
	 	//AND creating sustom info about socket connection, then we can catch it with 'socket.id'
	 	socket.emit('joinRoom', {
	 		name:name,
			room:room,
		 });
 	});

 	//listen to the custom 'message' event that we created at server.js
 	 socket.on('message', function (message) {
	 	console.log('Message text: ' + message.text);

	 	//TIMESTAMP
		var timestampMoment = moment.utc(message.timestamp);//timestamp value to moment
		var dateString = timestampMoment.local().format('h:mm:ss a');//moment formatted string

	    //Appending messages into div
	 	var $message = jQuery('.messages');//getting text from textField
	 	$message.append('<p><strong>' + message.name  + ' ' + dateString + '</strong></p>');//adding date and name
	 	$message.append('<p>' + message.text + '</p>');//showing data
	 	// jQuery('.messages').append('<p><strong>' + dateString + '</strong> ' + message.text + '</p>');

 	});




//Handle submitting of new message
var $form = jQuery('#message-form');

$form.on('submit', function (event) {
	event.preventDefault();//prevent refreshing page when button pressed

	$messageField = $form.find('input[name=message]');

	//Sending message with text extracted from form
	socket.emit('message' , {
		name: name,
		text: $messageField.val(), //.val() pull value out and return it as string
		timestamp: moment().valueOf()//timestamp in miliseconds
	});

	//Make field empty
	$messageField.val("");

	// var text = $('#textField').val();
	// $('#textField').val("");

});

////////////////////////////////////////////////
////////////////////////////////////////////////
////////////////////////////////////////////////
////////////////////////////////////////////////
//////////////////   WEBRTC   //////////////////
var callButton = document.getElementById('callButton');
var hangupButton = document.getElementById('hangupButton');
callButton.disabled = true;
hangupButton.disabled = true;
callButton.onclick = call;
hangupButton.onclick = hangup;

var isStarted = false;
var isInitiator = false;


function call() {
    console.log('Call button clicked');
    callButton.disabled = true;
    isInitiator = true;
    maybeStart();
}

function hangup() {
  console.log('Hangup button clicked');
  hangupButton.disabled = true;

  console.log('Hanging up.');
  stop();
  sendWebRTCMessage('bye');
}

var isTwoConnected = false;

function callButtonCheck() {
    //Turn on call button only if we have local stream and two sockets in the same chatroom
    if (isTwoConnected === true && typeof localStream !== 'undefined') {
        callButton.disabled = false;
    }
}

//////////////////////////////////////////////////////////////

socket.on('full', function () {
    console.log('\nChatroom is already full\n');
});

socket.on('ready', function () {
    console.log('\nTwo sockets connected, ready for call\n');
    isTwoConnected = true;
    callButtonCheck();
});

//////////////////////////////////////////////////////////////

var localVideo = document.querySelector('#localVideo');
var remoteVideo = document.querySelector('#remoteVideo');

var localStream;
var pc;
var remoteStream;

// var pcConfig = {
//   'iceServers': [{
//     'urls': 'stun:stun.l.google.com:19302'
//   }]
// };

/////////////////////////////////////
navigator.mediaDevices.getUserMedia({
  audio: false,
  video: true
})
.then(getLocalStream)
.catch(function(e) {
  alert('getUserMedia() error: ' + e.name);
});

function getLocalStream(stream) {
    console.log('Adding local stream.');
    localVideo.src = window.URL.createObjectURL(stream);
    localStream = stream;
    console.log('got user media');
    callButtonCheck();
    //   if (isInitiator) {
    // maybeStart();
//   }
}
/////////////////////////////////////



////////////////////////////////////////////////////////

function sendWebRTCMessage(message) {
  console.log('Client sending message: ', message);
  socket.emit('webrtcMessage', message);
}

// This client receives a message
socket.on('webrtcMessage', function(message) {
  console.log('Client received message:', message);
  if (message.type === 'offer' && !isInitiator) {
    if (!isStarted) {
      maybeStart();
    }
    console.log('Offer received, set it as Demote Description, message itself:', message);
    var remoteDescription = new RTCSessionDescription(message);
    console.log('Remote Description created: ', remoteDescription);
    pc.setRemoteDescription(remoteDescription);
    doAnswer();
  } else if (message.type === 'answer' && isStarted && isInitiator) {
    pc.setRemoteDescription(new RTCSessionDescription(message));
  } else if (message.type === 'candidate' && isStarted) {
    var candidate = new RTCIceCandidate({
      sdpMLineIndex: message.label,
      candidate: message.candidate
    });
    pc.addIceCandidate(candidate);
  } else if (message === 'bye' && isStarted) {
    handleRemoteHangup();
  }
});

////////////////////////////////////////////////////////



function maybeStart() {
  console.log('>>>>>>> maybeStart() ', isStarted, localStream);
  if (!isStarted && typeof localStream !== 'undefined' && isTwoConnected) {
    console.log('>>>>>> creating peer connection');
    createPeerConnection();
    pc.addStream(localStream);
    isStarted = true;
    console.log('isInitiator', isInitiator);
    if (isInitiator) {
      doCall();
    }
  }
}

window.onbeforeunload = function() {
  sendWebRTCMessage('bye');
};

/////////////////////////////////////////////////////////

function createPeerConnection() {
  try {
    pc = new RTCPeerConnection(null);
    pc.onicecandidate = handleIceCandidate;
    pc.onaddstream = handleRemoteStreamAdded;
    pc.onremovestream = handleRemoteStreamRemoved;
    console.log('Created RTCPeerConnnection');
  } catch (e) {
    console.log('Failed to create PeerConnection, exception: ' + e.message);
    alert('Cannot create RTCPeerConnection object.');
    return;
  }
}

function handleIceCandidate(event) {
  console.log('icecandidate event: ', event);
  if (event.candidate) {
    sendWebRTCMessage({
      type: 'candidate',
      label: event.candidate.sdpMLineIndex,
      id: event.candidate.sdpMid,
      candidate: event.candidate.candidate
    });
  } else {
    console.log('End of candidates.');
  }
}

function handleRemoteStreamAdded(event) {
  hangupButton.disabled = false;
  console.log('Hangup Button disability status:', hangupButton.disabled);

  console.log('Remote stream added.');
  remoteVideo.src = window.URL.createObjectURL(event.stream);
  remoteStream = event.stream;
}

function handleRemoteStreamRemoved(event) {
  console.log('Remote stream removed. Event: ', event);

}

function handleCreateOfferError(event) {
  console.log('createOffer() error: ', event);
}

function doCall() {
  console.log('Sending offer to peer');
  pc.createOffer(setLocalAndSendMessage, handleCreateOfferError);
}

function doAnswer() {
  console.log('Sending answer to peer.');
  pc.createAnswer().then(
    setLocalAndSendMessage,
    onCreateSessionDescriptionError
  );
}

function setLocalAndSendMessage(sessionDescription) {
  // Set Opus as the preferred codec in SDP if Opus is present.
  //  sessionDescription.sdp = preferOpus(sessionDescription.sdp);
  pc.setLocalDescription(sessionDescription);
  console.log('setLocalAndSendMessage sending message', sessionDescription);
  sendWebRTCMessage(sessionDescription);
}

function onCreateSessionDescriptionError(error) {
  // trace('Failed to create session description: ' + error.toString());
    console.log('Failed to create session description: ' + error.toString());
}

// function requestTurn(turnURL) {
//   var turnExists = false;
//   for (var i in pcConfig.iceServers) {
//     if (pcConfig.iceServers[i].url.substr(0, 5) === 'turn:') {
//       turnExists = true;
//       turnReady = true;
//       break;
//     }
//   }
//   if (!turnExists) {
//     console.log('Getting TURN server from ', turnURL);
//     // No TURN server. Get one from computeengineondemand.appspot.com:
//     var xhr = new XMLHttpRequest();
//     xhr.onreadystatechange = function() {
//       if (xhr.readyState === 4 && xhr.status === 200) {
//         var turnServer = JSON.parse(xhr.responseText);
//         console.log('Got TURN server: ', turnServer);
//         pcConfig.iceServers.push({
//           'url': 'turn:' + turnServer.username + '@' + turnServer.turn,
//           'credential': turnServer.password
//         });
//         turnReady = true;
//       }
//     };
//     xhr.open('GET', turnURL, true);
//     xhr.send();
//   }
// }

function handleRemoteHangup() {
  console.log('Session terminated.');
  stop();
  isInitiator = false;
}

function stop() {
  isStarted = false;
  // isAudioMuted = false;
  // isVideoMuted = false;
  pc.close();
  pc = null;
}



